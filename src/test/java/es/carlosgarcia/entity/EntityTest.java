package es.carlosgarcia.entity;

import org.junit.Assert;
import org.junit.Test;

public class EntityTest {

	@Test
	public void shouldValididateId() {
		Assert.assertFalse(Entity.isValidID(null));
		Assert.assertFalse(Entity.isValidID("0"));
		Assert.assertFalse(Entity.isValidID(""));
		Assert.assertFalse(Entity.isValidID(" "));
		
		Assert.assertTrue(Entity.isValidID("1"));
		Assert.assertTrue(Entity.isValidID("1-3-1"));
	}

}
