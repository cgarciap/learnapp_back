package es.carlosgarcia.entity;

import org.junit.Assert;
import org.junit.Test;

public class TranslateTest {

	@Test
	public void shouldWriteReadTag() {
		Translate translate = new Translate();

		translate.setLang("es");
		translate.setText("Hogar");
		
		Assert.assertEquals("es",    translate.getLang());
		Assert.assertEquals("Hogar", translate.getText());
		
		
		Translate    translate2 = new Translate("es", "Hogar");
		Assert.assertEquals("es",    translate2.getLang());
		Assert.assertEquals("Hogar", translate2.getText());
		
		Assert.assertTrue(translate.equals(translate2));
		Assert.assertTrue(translate.hashCode() !=  0);
	}
}
