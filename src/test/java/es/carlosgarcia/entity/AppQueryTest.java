package es.carlosgarcia.entity;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

public class AppQueryTest {

	@Test
	public void shouldWriteReadTag() {
		AppQuery q = new AppQuery();
		String   id = UUID.randomUUID().toString();
		q.setId(id);
		q.addTranslate(new Translate("es", "Hogar"));
		
		Assert.assertEquals(id , q.getId());
		Assert.assertEquals(1, q.getTranslates().size());
		Assert.assertTrue(q.toString().length() > 25);
		
	}
}
