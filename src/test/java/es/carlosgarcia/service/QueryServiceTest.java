package es.carlosgarcia.service;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.carlosgarcia.entity.AppQuery;
import es.carlosgarcia.repository.QueryRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:app-test.xml" })
public class QueryServiceTest {
    

	
	@Test
	public void shouldSaveOrUpdate() {
		QueryRepository queryRepository = Mockito.mock(QueryRepository.class);
		AppQuery		query   		= Mockito.mock(AppQuery.class);
		
		QueryService queryService = new QueryService(queryRepository);
		queryService.saveOrUpdate(query);
		
		// Verify that create method has been called
		Mockito.verify(queryRepository, Mockito.times(1)).saveOrUpdate(query);
	}
	
	@Test
	public void shouldGetAllQuestions() {
		QueryRepository queryRepository = Mockito.mock(QueryRepository.class);
		
		QueryService queryService = new QueryService(queryRepository);
		queryService.getQuestions();
		
		// Verify that create method has been called
		Mockito.verify(queryRepository, Mockito.times(1)).getAll();
	}
	
	@Test
	public void shouldGetOneQuestion() {
		QueryRepository queryRepository = Mockito.mock(QueryRepository.class);
		
		QueryService queryService = new QueryService(queryRepository);
		queryService.getQuestion("es", "en");

		// Verify that create method has been called
		Mockito.verify(queryRepository, Mockito.times(1)).getRandomWord("es", "en");
	}
	

	
	@Test
	public void shouldGetQuestionById() {
		QueryRepository queryRepository = Mockito.mock(QueryRepository.class);
		
		QueryService queryService = new QueryService(queryRepository);
		queryService.getQuestionByID("1");
		
		// Verify that create method has been called
		Mockito.verify(queryRepository, Mockito.times(1)).getById("1");
	}
	
	@Test
	public void shouldDeleteQuestionById() {
		QueryRepository queryRepository = Mockito.mock(QueryRepository.class);
		AppQuery		question   		= Mockito.mock(AppQuery.class);
		
		Mockito.when(question.getId()).thenReturn("1");
		Mockito.when(queryRepository.getById("1")).thenReturn(question);
		
		QueryService queryService = new QueryService(queryRepository);
		queryService.delete("1");
		
		// Verify that create method has been called
		Mockito.verify(queryRepository, Mockito.times(1)).getById("1");
		Mockito.verify(queryRepository, Mockito.times(1)).delete(question);
	}
	
	@Test
	public void shouldUpdateState() {
		QueryRepository queryRepository = Mockito.mock(QueryRepository.class);
		AppQuery		question   		= Mockito.mock(AppQuery.class);
		
		Mockito.when(question.getId()).thenReturn("1");
		Mockito.when(queryRepository.getById("1")).thenReturn(question);
		
		QueryService queryService = new QueryService(queryRepository);
		queryService.updateState("1", true);
		queryService.updateState("1", false);
		
		// Verify that create method has been called
		Mockito.verify(queryRepository, Mockito.times(2)).getById("1");
		Mockito.verify(queryRepository, Mockito.times(2)).saveOrUpdate(question);
	}

}
