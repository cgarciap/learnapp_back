package es.carlosgarcia.api;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;

@RunWith(MockitoJUnitRunner.class)
public class TokenUtilsTest {
	
	@Mock
	private UserDetails userDetails;
	
	private long 		expiresAt;
	
	@Before
	public void setup(){
//		Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
//		authorities.add(e)
		Mockito.when(userDetails.getUsername()).thenReturn("admin");
		Mockito.when(userDetails.getPassword()).thenReturn("123456");
		Mockito.when(userDetails.getAuthorities()).thenReturn(null);
		
		this.expiresAt   = System.currentTimeMillis() + 10000;	
	}
	
	
	@Test
	public void shouldGenerateToken(){
		String		accessToken = TokenUtils.createToken(userDetails, expiresAt);
		Assert.assertTrue(StringUtils.length(accessToken) >= 5);
	}
	
	@Test
	public void shouldValidateToken(){
		String		accessToken = TokenUtils.createToken(userDetails, expiresAt);
		Assert.assertTrue(TokenUtils.validateToken(accessToken, userDetails));
	}
	
	@Test
	public void shouldNotValidateExpiredToken(){
		long		expiresAt   = System.currentTimeMillis() - 1000;
		String		accessToken = TokenUtils.createToken(userDetails, expiresAt);
		Assert.assertFalse(TokenUtils.validateToken(accessToken, userDetails));
	}
	
	@Test
	public void shouldGetUserNameFromToken(){
		 String		accessToken = TokenUtils.createToken(userDetails, expiresAt);
		 Assert.assertEquals("admin", TokenUtils.getUserNameFromToken(accessToken));
	}
}
