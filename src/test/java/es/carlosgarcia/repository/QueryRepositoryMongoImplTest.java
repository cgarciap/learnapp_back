package es.carlosgarcia.repository;

import java.util.List;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfig;
import de.flapdoodle.embed.mongo.distribution.Version;
import es.carlosgarcia.entity.AppQuery;
import es.carlosgarcia.entity.Translate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:app-test.xml" })
public class QueryRepositoryMongoImplTest {

	private static int PORT = 37017;

	@Resource
	private QueryRepositoryMongoImpl queryRepository;

	private static MongodExecutable mongoExecutable;
	private static MongodProcess mongoProcess;
	
	@Resource
	private MongoTemplate template;
	 
	 
	@BeforeClass
	public static void beforeAllEach() throws Exception {
		MongodStarter starter = MongodStarter.getDefaultInstance();
		mongoExecutable = starter.prepare(new MongodConfig(Version.V2_2_0, QueryRepositoryMongoImplTest.PORT, false));
		mongoProcess = mongoExecutable.start();
	}

	@AfterClass
	public static void afterAll() throws Exception {
//		mongoProcess.stop();
//		mongoExecutable.stop();
	}
	
    @Before  
    public void createDocuments(){  
        DBObject[] records = new BasicDBObject[6];  
        records[0] = (DBObject) JSON.parse("{_id: '1', state: 1, translates: [{lang: 'es', text: 'perro'},    {lang: 'en', text: 'dog'}]}");
        records[1] = (DBObject) JSON.parse("{_id: '2', state: 1, translates: [{lang: 'es', text: 'cama'},     {lang: 'en', text:  'bed'}]}");
        records[2] = (DBObject) JSON.parse("{_id: '3', state: 1, translates: [{lang: 'es', text: 'avión'}, 	  {lang: 'en', text: 'plane'}]}");
        records[3] = (DBObject) JSON.parse("{_id: '4', state: 1, translates: [{lang: 'es', text: 'teclado'},  {lang: 'en', text: 'keyboard'}]}");
        records[4] = (DBObject) JSON.parse("{_id: '5', state: 1, translates: [{lang: 'es', text: 'ratón'}, 	  {lang: 'en', text: 'mouse'}]}");
        records[5] = (DBObject) JSON.parse("{_id: '6', state: 1, translates: [{lang: 'es', text: 'pantalla'}, {lang: 'pt', text: 'tela'}]}");
        
        String collectionName = template.getCollectionName(AppQuery.class);
        for (int i = 0; i < records.length; i++){  
        	template.insert(records[i], collectionName);
        }  
    }  
	
    @After  
    public void deleteDocuments(){  
		template.remove(new Query(), AppQuery.class);
    }  
    
	@Test
	public void shouldGetById() {
		Assert.assertNotNull(this.queryRepository.getById("1"));
	}

	@Test
	public void shouldGetAll() {
		List<AppQuery> all = this.queryRepository.getAll();
		Assert.assertTrue(all.size() == 6);
	}
	
	@Test
	public void shouldCount() {
		long recordCount = this.queryRepository.getCount();
		Assert.assertEquals(6, recordCount);
	}
	
	@Test
	public void shouldGetRandomWord() {
		AppQuery appQuery  = this.queryRepository.getRandomWord("es", "en");
		Assert.assertNotNull(appQuery);
		Assert.assertEquals("AppQuery on invalid estate", appQuery.getState(), Integer.valueOf(1));

		
		List<Translate> translates = appQuery.getTranslates();
		boolean findedES = false;
		boolean findedEN = false;
		for (Translate t : translates){
			if (! findedES){
				findedES = t.getLang().equalsIgnoreCase("es");
			}
			if (! findedEN){
				findedEN = t.getLang().equalsIgnoreCase("en");
			}
		}

		Assert.assertTrue("They have not been found words with necessary translations.", (findedES && findedEN));
	}
	
	@Test
	public void shouldGetWordsByTranslates() {
		Assert.assertEquals(1, this.queryRepository.getWordCount("es", "pt"));
		Assert.assertEquals(1, this.queryRepository.getWordCount("pt", "es"));
		Assert.assertEquals(5, this.queryRepository.getWordCount("es", "en"));
	}

	
	@Test
	public void shouldCreateNewAppQuery() {
		long recordCount = this.queryRepository.getCount();
		
		AppQuery question = new AppQuery();
		question.setState(1);
		
		this.queryRepository.saveOrUpdate(question);
		
		recordCount = this.queryRepository.getCount();
		Assert.assertEquals(7, recordCount);
	}
	
	
	@Test
	public void shouldDelete() {
		Assert.assertNotNull(this.queryRepository.getById("1"));
		
		AppQuery question = new AppQuery();
		question.setId("1");
		this.queryRepository.delete(question);
		
		Assert.assertNull(this.queryRepository.getById("1"));
		Assert.assertEquals(5, this.queryRepository.getCount());
	}
}
