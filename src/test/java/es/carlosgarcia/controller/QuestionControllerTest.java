package es.carlosgarcia.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import es.carlosgarcia.entity.AppQuery;
import es.carlosgarcia.service.QueryService;

@RunWith(MockitoJUnitRunner.class)
public class QuestionControllerTest {

	@Mock
	private QueryService queryService;

	@Test
	public void shouldGetAllQuestions(){
		QuestionController  controller = new QuestionController(queryService);
		controller.getAll();
		Mockito.verify(queryService, Mockito.only()).getQuestions();
	}
	
	@Test
	public void shouldCreateQuestion(){
		AppQuery question = Mockito.mock(AppQuery.class);
		Mockito.when(question.getId()).thenReturn(null);

		QuestionController controller = new QuestionController(queryService);
		controller.createQuestion(question);

		Mockito.verify(queryService, Mockito.only()).saveOrUpdate(question);
	}

	@Test
	public void shouldGetQuestion(){
		AppQuery questionToReturn = Mockito.mock(AppQuery.class);
		Mockito.when(queryService.getQuestion("es", "en")).thenReturn(questionToReturn);

		QuestionController  controller = new QuestionController(queryService);
		AppQuery question = controller.getQuestion("es", "en");

		Mockito.verify(queryService, Mockito.only()).getQuestion("es", "en");
		Assert.assertNotNull(question);
	}
	
	@Test
	public void shouldUpdateQuestion(){
		AppQuery question = Mockito.mock(AppQuery.class);

		QuestionController controller = new QuestionController(queryService);
		controller.updateQuestion("1", question);

		Mockito.verify(queryService, Mockito.only()).saveOrUpdate(question);
	}	
	
	@Test
	public void shouldDeleteQuestion(){
		QuestionController controller = new QuestionController(queryService);
		controller.deleteQuestion("1");
		Mockito.verify(queryService, Mockito.only()).delete("1");
	}
	
	@Test
	public void shouldSetStateOn(){
		QuestionController controller = new QuestionController(queryService);
		controller.setStateOn("1");
		Mockito.verify(queryService, Mockito.only()).updateState("1", true);
	}
	
	@Test
	public void shouldSetStateOff(){
		QuestionController controller = new QuestionController(queryService);
		controller.setStateOff("1");
		Mockito.verify(queryService, Mockito.only()).updateState("1", false);
	}
	
	@Test
	public void shouldGetByID(){
		QuestionController controller = new QuestionController(queryService);
		controller.getQuestionByID("1");
		Mockito.verify(queryService, Mockito.only()).getQuestionByID("1");
	}	
}
