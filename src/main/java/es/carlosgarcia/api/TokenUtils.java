package es.carlosgarcia.api;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Hex;

public class TokenUtils {
	
	private static final String MAGIC_KEY  = "appObfuscateSecretKey";


	private static final MessageDigest messageDigest;
	
	static {
       try {
           messageDigest = MessageDigest.getInstance("MD5");
       } catch (NoSuchAlgorithmException e) {
           throw new IllegalStateException("No MD5 algorithm available!");
       }
	}	
	

	public static String createToken(UserDetails userDetails, long expiresAt) {


		StringBuilder tokenBuilder = new StringBuilder(64);
		tokenBuilder.append(userDetails.getUsername());
		tokenBuilder.append(":");
		tokenBuilder.append(expiresAt);
		tokenBuilder.append(":");
		tokenBuilder.append(TokenUtils.computeSignature(userDetails, expiresAt));

		return tokenBuilder.toString();
	}
	
	public static String computeSignature(UserDetails userDetails, long expires) {
		String signature = TokenUtils.createSignature(userDetails, expires);
		byte[] bytes     = messageDigest.digest(signature.getBytes());
		String encoded   = new String(Hex.encode(bytes));
		return encoded;
	}
	

	public static String getUserNameFromToken(String authToken) {
		return TokenUtils.getTokenAt(authToken, 0);
	}

	public static boolean validateToken(String authToken, UserDetails userDetails) {
		long tokenExpiresAt = Long.parseLong(TokenUtils.getTokenAt(authToken, 1));

		if (tokenExpiresAt < System.currentTimeMillis()) {
			return false;
		}

		String okSignature    = TokenUtils.computeSignature(userDetails, tokenExpiresAt);
		String tokenSignature = TokenUtils.getTokenAt(authToken, 2);

		return tokenSignature.equals(okSignature);
	}
	
	private static String getTokenAt(String authToken, int idx) {
		String[] parts = authToken.split(":");
		return parts[idx];
	}
	
	private static String createSignature(UserDetails userDetails, long expires){
		StringBuilder signatureBuilder = new StringBuilder(64);
		signatureBuilder.append(userDetails.getUsername());
		signatureBuilder.append(":");
		signatureBuilder.append(expires);
		signatureBuilder.append(":");
		signatureBuilder.append(userDetails.getPassword());

		signatureBuilder.append(":");
		signatureBuilder.append(TokenUtils.MAGIC_KEY); // Por que este código sólo se usa en este punto
		return signatureBuilder.toString();
	}
}