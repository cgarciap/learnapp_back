package es.carlosgarcia.api;


import org.springframework.http.HttpStatus;

/**
 * Rest api response.
 * @author Carlos García
 */
public class RestApiResponse {
	
    private final HttpStatus httpStatusCode;
    private final ApiCode apiCode;
    private final String message;

    public RestApiResponse(HttpStatus httpStatusCode, ApiCode apiCode, String message) {
        this.httpStatusCode = httpStatusCode;
        this.apiCode = apiCode;
        this.message = message;
    }

    public HttpStatus getHttpStatusCode() {
        return this.httpStatusCode;
    }
 
    public ApiCode getApiCode() {
        return this.apiCode;
    }

    public String getMessage() {
        return this.message;
    }
}

