package es.carlosgarcia.api;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * Helper method 
 * @author Carlos García
 * @see http://carlos-garcia.es
 */
public class ApiHelper {
	
	public static ResponseEntity<RestApiResponse> createAndSendResponse(RestApiResponse response) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Cache-Control", "no-store");
		headers.set("Pragma", "no-cache");
		headers.setContentType(MediaType.APPLICATION_JSON);
		ResponseEntity<RestApiResponse> responseEntity = new ResponseEntity<RestApiResponse>(response, headers, response.getHttpStatusCode());
		return responseEntity;
	}
	
	public static ResponseEntity<RestApiResponse> createAndSendOKResponse(){
        return ApiHelper.createAndSendOKResponse("");
	}	
	
	public static ResponseEntity<RestApiResponse> createAndSendOKResponse(String data){
        RestApiResponse restApiError = new RestApiResponse(HttpStatus.OK, ApiCode.OK, data);  
        return ApiHelper.createAndSendResponse(restApiError);  
	}
	
}
