package es.carlosgarcia.api;

/**
 * Defined application codes.
 * @author Carlos García
 */
public enum ApiCode {
	OK,
	NOT_FOUND,
	SECURITY,
	ACCESS_DENIED,
	GENERIC
}
