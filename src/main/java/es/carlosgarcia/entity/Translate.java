package es.carlosgarcia.entity;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Translate {

	@NotNull
	private String lang;

	@NotNull 
	private String text;
	
	public Translate(){
		super();
	}
	
	public Translate(String lang, String text) {
		super();
		this.lang = lang;
		this.text = text;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String langCode) {
		this.lang = langCode;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}
