package es.carlosgarcia.entity;

public enum StatusEnum {
	PENDING, PUBLISHED, CANCELED
}