package es.carlosgarcia.entity;

public abstract class Entity {
	
	
	public static boolean isValidID(String id){
		boolean isOK = (id != null);
		
		if (isOK){
			String id2 = id.trim();
			isOK       = (! "0".equalsIgnoreCase(id2)) && (id2.length() > 0);
		}
		
		return isOK;
	}
}
