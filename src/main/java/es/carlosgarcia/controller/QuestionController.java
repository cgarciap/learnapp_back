package es.carlosgarcia.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.carlosgarcia.api.ApiHelper;
import es.carlosgarcia.api.RestApiResponse;
import es.carlosgarcia.entity.AppQuery;
import es.carlosgarcia.service.QueryService;

/**
 * Question Controler
 * 
 * @author Carlos García.
 */
@RestController
@RequestMapping(value = "/v1/")
public class QuestionController  {
	private static final Logger logger = LoggerFactory.getLogger(QuestionController.class);
	
	private QueryService queryService;
	
	@Autowired
	public QuestionController(QueryService queryService) {
		super();
		this.queryService = queryService;
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/api/questions", method = RequestMethod.GET)
	public List<AppQuery> getAll() {
		logger.trace("Devuelve todas las preguntas");
		return queryService.getQuestions();
	}
	

	@RequestMapping(value = "/api/questions", method = RequestMethod.POST)
	public ResponseEntity<RestApiResponse> createQuestion(@RequestBody AppQuery query) {
		if (logger.isTraceEnabled()){
			logger.trace("Creando una nueva pregunta");
			logger.trace(query.toString());
		}
		
		queryService.saveOrUpdate(query);
		return ApiHelper.createAndSendOKResponse(query.getId());
	}
	
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/api/questions/{id}", method = RequestMethod.PUT)
	public ResponseEntity<RestApiResponse> updateQuestion(@PathVariable String id, @RequestBody AppQuery query) {
		if (logger.isTraceEnabled()){
			logger.trace("Actualizando la pregunta con id: {}", query.getId());
			logger.trace(query.toString());
		}
		
		queryService.saveOrUpdate(query);
		return ApiHelper.createAndSendOKResponse(query.getId());
	}
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/api/questions/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<RestApiResponse> deleteQuestion(@PathVariable String id) {
		logger.trace("Eliminando la pregunta con id: {}", id);
		queryService.delete(id);
		return ApiHelper.createAndSendOKResponse(id);
	}
	
	@RequestMapping(value = "/api/questions/{nativeLang}/{learnLang}", method = RequestMethod.GET)
	public AppQuery getQuestion(@PathVariable String nativeLang, @PathVariable String learnLang) {
		logger.trace("Devuelve una question que tenga las traduciones: {}, {}", nativeLang, learnLang);
		
		AppQuery appQuery = queryService.getQuestion(nativeLang, learnLang);
//		if (appQuery == null){
//			throw new NoSuchElementException("no_words");
//		}
		return appQuery;
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/api/questions/{id}/state/on", method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<RestApiResponse> setStateOn(@PathVariable String id) {
		queryService.updateState(id, true);
		return ApiHelper.createAndSendOKResponse(this.toJSON(id, true));
	}
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/api/questions/{id}/state/off", method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<RestApiResponse> setStateOff(@PathVariable String id) {
		queryService.updateState(id, false);
		return ApiHelper.createAndSendOKResponse(this.toJSON(id, false));
	}
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/api/questions/id/{id}", method = RequestMethod.GET)
	public AppQuery getQuestionByID(@PathVariable String id) {
		logger.trace("Devuelve la question con ID: {}", id);
		return queryService.getQuestionByID(id);
	}
	
	private String toJSON(String id, boolean state){
		int		   stateInt = state?1:0;
		JSONObject jsonObj  = new JSONObject();
		jsonObj.put("id",    id);
		jsonObj.put("state", stateInt);
		return jsonObj.toString();
	}
}

