package es.carlosgarcia.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import es.carlosgarcia.api.ApiCode;
import es.carlosgarcia.api.ApiHelper;
import es.carlosgarcia.api.RestApiResponse;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<RestApiResponse>  handleAuthenticationException(AccessDeniedException exception, HttpServletRequest request, HttpServletResponse response){
		RestApiResponse restApiError = new RestApiResponse(HttpStatus.UNAUTHORIZED, ApiCode.ACCESS_DENIED, exception.getMessage());  
		return ApiHelper.createAndSendResponse(restApiError);  
	}
	


	@ExceptionHandler(Exception.class)  
	protected ResponseEntity<RestApiResponse> handleException(Exception exception, HttpServletRequest request, HttpServletResponse response) {  
		RestApiResponse restApiError = new RestApiResponse(HttpStatus.BAD_REQUEST, ApiCode.GENERIC, exception.getMessage());  
		return ApiHelper.createAndSendResponse(restApiError);  
	}  
}
