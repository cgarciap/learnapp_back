package es.carlosgarcia.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.carlosgarcia.api.TokenUtils;
import es.carlosgarcia.entity.TokenTransfer;

@RestController
public class AuthenticationController {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
	private static final long   EXPIRED_MILLISECONDS = 2 * 1000 * 60 * 60L; // Caduca en dos horas
	
	@Resource
	private UserDetailsService userService;

	@Resource(name="authenticationManager")
	private AuthenticationManager authenticationManager;
	
	@RequestMapping(value = "/v1/api/authenticate", method = RequestMethod.POST)
	public TokenTransfer authenticate(@RequestParam("username") String username, @RequestParam("password") String password) {
		logger.info("Procedemos a autenticar al usuario {}", username);
		
		
		try {
			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
			Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
			
			logger.info("Autenticando correcta del usuario {}", username);
			
			SecurityContextHolder.getContext().setAuthentication(authentication);
	
			UserDetails userDetails = this.userService.loadUserByUsername(username);
			long		expiresAt   = System.currentTimeMillis() + EXPIRED_MILLISECONDS;	
			String		token		= TokenUtils.createToken(userDetails, expiresAt);

			return new TokenTransfer(token);
		} catch (AuthenticationException ex){
			logger.info("Autenticando fallida del usuario {} - {}", username, ex.toString());
			throw ex;
		}
	}
}
