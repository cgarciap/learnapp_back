package es.carlosgarcia.repository;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import es.carlosgarcia.entity.AppQuery;
import es.carlosgarcia.entity.Entity;
import es.carlosgarcia.entity.StatusEnum;


@Repository
public class QueryRepositoryMongoImpl implements QueryRepository {
	private final Logger logger = LoggerFactory.getLogger(QueryRepositoryMongoImpl.class);
	private static final long serialVersionUID = -4599832253548800012L;

	private MongoTemplate template;
	
	@Autowired
	public QueryRepositoryMongoImpl(MongoTemplate template){
		this.template = template; 
	}
	
	
	@Override
	public long getCount() {
		logger.trace("getCount()");
		return template.count(new Query(), AppQuery.class);
	}
	
	private int getRandomInt(long maxInt){
	    Random rand = new Random();
	    return rand.nextInt((int) maxInt);
	}
	
	@Override
	public AppQuery getRandomWord(String nativeLang, String learnLang){
		logger.trace("getRandomWord: {}, {}", nativeLang, learnLang);
		
		long queryCount = this.getWordCount(nativeLang, learnLang);
		
		if (queryCount == 0){
			logger.trace("Ninguna pregunta cumple esas condiciones: {}, {}", nativeLang, learnLang);
			return null;
		}
		
		int	  skip  = this.getRandomInt(queryCount);
		Query query = this.prepareQuery(nativeLang, learnLang);
		query.skip(skip).limit(1);
		
		List<AppQuery> questions = template.find(query, AppQuery.class);
		AppQuery	   question	 = questions.get(0);

		logger.trace("Pregunta seleccionada con ID {}", question.getId());
		
		return question;
	}
	
	public long getWordCount(String nativeLang, String learnLang){
		logger.trace("getWordCount: {}, {}", nativeLang, learnLang);
		
		Query query = this.prepareQuery(nativeLang, learnLang);
		return template.count(query, AppQuery.class);
	}
	

	@Override
	public void saveOrUpdate(AppQuery appQuery) {
		if (! Entity.isValidID(appQuery.getId())){
			appQuery.setId(UUID.randomUUID().toString());
			logger.trace("Estableciendo el ID: {}, {}", appQuery.getId());
		} 
		template.save(appQuery);
	}

	@Override
	public void delete(AppQuery appQuery) {
		logger.trace("Deleting question with ID: {}", appQuery.getId());
		template.remove(appQuery);
	}

	@Override
	public List<AppQuery> getAll() {
		logger.trace("getAll()");
		return template.findAll(AppQuery.class);
	}

	
	@Override
	public AppQuery getById(String id) {
		logger.trace("getById {}:", id);
		
		Criteria criteria = Criteria.where("_id").is(id);
		Query    query    = Query.query(criteria);
		return template.findOne(query, AppQuery.class);
	}
	
	private Query prepareQuery(String nativeLang, String learnLang){
		Criteria criteria = Criteria.where("translates.lang").all(nativeLang, learnLang);
		criteria.and("state").is(StatusEnum.PUBLISHED.ordinal());
		return Query.query(criteria);
	}
}
