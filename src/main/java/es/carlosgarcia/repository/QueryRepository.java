package es.carlosgarcia.repository;

import java.util.List;

import es.carlosgarcia.entity.AppQuery;

public interface QueryRepository extends java.io.Serializable {
	AppQuery getById(String id);
	long getCount();
	void saveOrUpdate(AppQuery appQuery);
	void delete(AppQuery appQuery);
	AppQuery getRandomWord(String nativeLang, String learnLang);
	long getWordCount(String nativeLang, String learnLang);
	List<AppQuery> getAll();
}
