package es.carlosgarcia.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.carlosgarcia.entity.AppQuery;
import es.carlosgarcia.repository.QueryRepository;

@Service
public class QueryService {
	private static final Logger logger  = LoggerFactory.getLogger(QueryService.class);

	private QueryRepository queryRepository;
	
	@Autowired
	public QueryService(QueryRepository queryRepository){
		logger.trace("QueryService");
		this.queryRepository = queryRepository;
	}

	public AppQuery getQuestion(String nativeLang, String learnLang)  {
		return queryRepository.getRandomWord(nativeLang, learnLang);
	}
	
	public AppQuery getQuestionByID(String id)  {
		return queryRepository.getById(id);
	}
	
	public List<AppQuery> getQuestions()  {
		return queryRepository.getAll();
	}
	
	public void saveOrUpdate(AppQuery question)  {
		queryRepository.saveOrUpdate(question);
	}
	
	public void delete(String queryID)  {
		AppQuery question = this.getByIdOrThrowException(queryID);
		queryRepository.delete(question);
	}
	
	
	public void updateState(String queryID, boolean isStateOn){
		AppQuery question = this.getByIdOrThrowException(queryID);
		int		 state	  = isStateOn?1:0;
		question.setState(state);
		queryRepository.saveOrUpdate(question);
	}
	
	private AppQuery getByIdOrThrowException(String queryID){
		AppQuery question = queryRepository.getById(queryID);
		if (question == null){
			throw new IllegalArgumentException("Question not found " + queryID);
		}
		return question;
	}
}