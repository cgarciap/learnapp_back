package es.carlosgarcia.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import es.carlosgarcia.api.TokenUtils;

@Component
public class AuthenticationTokenProcessingFilter extends GenericFilterBean {

	private final UserDetailsService userService;

	@Autowired
	public AuthenticationTokenProcessingFilter(UserDetailsService userService){
		this.userService = userService;
	}

	@Override
	public void doFilter(ServletRequest request,
						 ServletResponse response,
						 FilterChain chain) throws IOException, ServletException {
		String userName	 = null;
		String authToken = this.getAuthToken((HttpServletRequest) request);
		
		if (StringUtils.isNotEmpty(authToken)){
			userName = TokenUtils.getUserNameFromToken(authToken);
		}

		if (userName != null) {
			UserDetails userDetails = this.userService.loadUserByUsername(userName);
			
			if (TokenUtils.validateToken(authToken, userDetails)) {
				Authentication auth = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				SecurityContextHolder.getContext().setAuthentication(auth);
			}
		}

		chain.doFilter(request, response);
	}


	private String getAuthToken(HttpServletRequest httpRequest){
		String authToken = httpRequest.getHeader("X-Auth-Token");
		if (authToken == null) {
			authToken = httpRequest.getParameter("token");
		}
		return authToken;
	}
}